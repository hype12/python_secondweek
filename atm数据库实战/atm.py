#自动取款机 登陆、退出、查询余额、存款、取款

from db.dbModel import Model


#存储登录账户的信息


if __name__ == '__main__':
    acc = Model("account")
    log = Model("oplog")
    cid = ''
    pwd = ''

    client = {}

    print("="*20,"银行账户管理系统","="*20)
    print("\t1.登录账户\t\t2.退出账户")
    print("\t3.存款    \t\t4.取款")
    print("\t5.余额查询\t\t6.查看日志")
    print("="*58)

    while True:
        index = input("请输入操作序号：")

    #登录账户
        if index =='1':

            print('供测试账户如下：')
            for v in acc.getData(limit='3'):
                print(v)
            print("="*24,"登陆账户","="*24)
            cid = input("账号：")
            pwd = input("密码：")
            
            dlist=acc.getData(where=["accnum='%s'"%cid])
            if len(dlist)>0:
                client=dlist[0]
                
            if client and client["password"] == pwd:
                print("登录成功！")
                log.add({'logtext':'%s尝试登录系统'%cid,'accountid':client['id'] })
            else:
                print("登录失败！")
                cid = ''
                pwd = ''
                client = {}
            print("="*58)
    #退出账户       
        elif index =='2':
            print("="*24,"退出账户","="*24)
            if cid == '':
                print("您尚未登录账户!")
            else:
                log.add({'logtext':'%s退出系统'%cid,'accountid':client['id'] })
                cid = ''
                pwd = ''
                client = {}
                print("退出成功！")
                break
            print("="*58)
    #存款
        elif index =='3':
            print("="*24,"存款操作","="*24)
            print(client)
            if cid == '':
                print("您尚未登录账户!")
            else:
                m = float(input("请输入存款金额："))
                client["money"] += m
                #print(client)
                acc.save(client)
                print("存款成功！")
                log.add({'logtext':'%s存款成功，存入%.2f元'%(cid,m),'accountid':client['id'] })
            print("="*58)  
    #取款    
        elif index =='4':
            print("="*24,"取款操作","="*24)
            print(client)
            if cid == '':
                print("您尚未登录账户!")
            else:
                m = float(input("请输入取款金额："))
                if client["money"]<m:
                    print('账户余额不足！')
                else:
                    client["money"] -= m
                    print(client)
                    acc.save(client)
                    print("取款成功！")
                    log.add({'logtext':'%s取款成功，取出%.2f元'%(cid,m),'accountid':client['id'] })
            print("="*58)
    #余额查询        
        elif index =='5':
            print("="*24,"余额查询","="*24)
            if cid == '':
                print("您尚未登录账户!")
            else:
                m = client["money"]
                print("您的账户余额为：",m)
                log.add({'logtext':'%s余额查询成功！'%(cid),'accountid':client['id'] })
            print("="*58) 
    #操作日志查询        
        elif index =='6':
            print("="*24,"操作日志查询","="*24)
            if cid == '':
                print("您尚未登录账户!")
            else:
                print("您的账户操作日志如下:")
                for a in log.getData(where=["accountid='%s'"%client['id']],order=" opdate desc"):
                    print('操作时间：{0},操作日志内容：{1}'.format(a['opdate'],a['logtext']))
            print("="*58)