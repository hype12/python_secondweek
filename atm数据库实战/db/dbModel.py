#数据库操作model类

import pymysql

#导入模块的配置文件
from db import config

class Model:
	'''先定义好几个属性'''
	t=None #表名称
	l=None #数据库连接
	c=None #游标
	key=None #表主键
	fields=[] #表字段集合

	#构造方法进行数据库连接
	def __init__(self,t,config=config):
		try:
			#创建数据库连接
			self.l=pymysql.connect(host=config.HOST,port=config.PORT,user=config.USER,db=config.DB,password=config.PWD,charset=config.CHARSET)
			#创建游标  以字典格式返回数据内容pymysql.cursors.DictCursor
			self.c=self.l.cursor(pymysql.cursors.DictCursor) 
			#指定操作表
			self.t=t
			#初始化fields和key
			self.__getTableFields()
		except Exception as err:
			print('model初始化错误，数据库连接失败%s'%(err))
	#定义动态获取表的主键和列字段函数
	def __getTableFields(self):
		#定义查询表字段的sql
		sql="show columns from {:s}".format(self.t)
		#查询字段列表
		self.c.execute(sql)
		flist=self.c.fetchall()
		#print(flist)
		#遍历返回结果插入fields
		for v in flist:
			self.fields.append(v['Field'])
			if v['Key']=='PRI':
				self.key=v['Field']
	#定义表的增删改查的方法
	#新增表数据
	def add(self,data={}):
		try:
			#判断data是否为空
			if data:
				keys=[]
				values=[]
				for k,v in data.items():
					if k in self.fields:
						keys.append(k)
						values.append(v)
				#拼接sql语句
				sql="insert into %s(%s) values(%s)"%(self.t,','.join(keys),','.join(['%s']*len(values)))
				# print(sql)
				#执行sql操作
				self.c.execute(sql,tuple(values))
				#事务提交
				self.l.commit()
				#返回插入数据的id
				return self.c.lastrowid
		except Exception as err:
			print("SQL修改执行错误，原因：%s" % err)
			return 0
	#修改数据
	def save(self,data={}):
		try:
			#判断data是否为空
			if data:
				values=[]
				for k,v in data.items():
					if k in self.fields and k!=self.key:
						values.append("%s='%s'"%(k,v))
				#拼接sql语句
				sql="update %s set %s where %s='%s'"%(self.t,','.join(values),self.key,data.get(self.key))
				print(sql)
				#执行sql操作
				self.c.execute(sql)
				#事务提交
				self.l.commit()
				#返回插入数据的id
				return self.c.rowcount
		except Exception as err:
			print("SQL修改执行错误，原因：%s" % err)
			return 0
	#删除数据
	def delete(self,id=0):
		try:
			#判断id是否存在
			if id:
				#拼接sql语句
				sql="delete from %s where %s='%s'"%(self.t,self.key,id)
				#事务提交
				self.l.commit()
				return self.c.rowcount
		except Exception as err:
			print("SQL修改执行错误，原因：%s" % err)
			return 0
	#查询所有数据
	def getAll(self):
		try:
			#定义查询语句
			sql="select * from %s"% self.t
			#执行查询
			self.c.execute(sql)
			#遍历输出
			dlist=self.c.fetchall()
			return dlist
		except Exception as err:
			print("SQL修改执行错误，原因：%s" % err)
			return []
	#查询某条数据
	def getOne(self,id):
		try:
			#定义查询语句
			sql="select * from %s where %s='%s'"%(self.t,self.key,id)
			#执行查询
			self.c.execute(sql)
			#遍历输出
			return self.c.fetchone()
		except Exception as err:
			print("SQL修改执行错误，原因：%s" % err)
			return {}
	#带着参数查询
	def getData(self,where=[],order=None,limit=None):
		try:
			#判断参数的值
			if isinstance(where,list) and len(where)>0:
				where='where '+' '.join(where)
			else:
				where=''
			if order is not None:
				order=' order by '+order
			else:
				order=''
			if limit is not None:
				limit="limit "+limit
			else:
				limit=''
			#定义查询语句
			sql="select * from %s %s %s %s "%(self.t,where,order,limit)
			# print(sql)
			#执行查询
			self.c.execute(sql)
			#遍历输出
			dlist=self.c.fetchall()
			return dlist
		except Exception as err:
			print("SQL修改执行错误，原因：%s" % err)
			return []
	#析构方法关闭数据库
	def __del__(self):
		if self.l:
			self.l.close()
		if self.c:
			self.c.close()
		#print('数据库连接已经关闭！')


