1. 在users表中查询注册时间最早的十条会员信息。 
select * from users order by  cdate asc limit 10;
2. 从两个表中查询点赞数最高的5条博客信息，要求显示字段： 
（博文id，标题，点赞数，会员名） 
select b.id,b.title,b.pcount,u.name from blog b,users u where u.id=b.uid and b.flag='1' order by b.pcount desc  limit 5;

3. 统计每个会员的发表博文数量（降序），要求显示字段（会员id号，姓名，博文数量） 
select u.id,u.name,b.c from users u,(select count(1) as c,uid from blog where flag='1' group by uid) b where u.id=b.uid order by b.c desc;

4. 获取会员的博文平均点赞数量最高的三位。显示字段（会员id，姓名，平均点赞数） 
 select u.id,u.name,b.avgc from users u,(select avg(pcount) avgc,uid from blog  where flag='1' group by uid) b where u.id=b.uid order by b.avgc desc limit 3;
5. 删除没有发表博文的所有会员信息。 
delete from  users where id not in (select uid from blog );
